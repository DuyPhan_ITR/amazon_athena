# PREPARING DATA FOR THE MODEL

## 1. Create necessary data from amazon athena:
* The code for the creating data requirement can be access **[here](https://gitlab.com/DuyPhan_ITR/amazon_athena/-/blob/Phuong_Anh/SQL_Solution/Requirement_data.md)**
* The code for the creating data specific can be access **[here](https://gitlab.com/DuyPhan_ITR/amazon_athena/-/blob/Phuong_Anh/SQL_Solution/Prepare_dataset.md)**
* Save the data into the **Data folder**

## 2. Run the *main.ipynb* file
* This can be run by the jupyter notebook
* The data path and data length must be configed
```python
data_requirement  #path of the requirements data (in csv)
data_specific #path of the specfic data (in csv) like: QRS_HR, Afib, SVEs,VEs
save_dir #path to save the data
data_length #length of the data 
```

## 3. Save the data
The data after processing will be saved in **Data_after_processing folder** 
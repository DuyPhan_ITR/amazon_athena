#  Right- wrong percent for each class
```sql
with all_events_v1 as (
        select all_events.*, all_events_params.eventid, 
            all_events_params.recommendedlead,
            all_events_params.channel0interpretation,
            all_events_params.channel1interpretation,
            all_events_params.channel2interpretation
        from all_events 
        inner join all_events_params on all_events.id = all_events_params.eventid
    ),
all_events_v2 as (
        select *
        from all_events_v1
        inner join reports on all_events_v1.studyfid = reports.studyfid
        where reports.studytype = 'MCT'
    ),
all_events_v2_2 as(
        select 
            type,recommendedlead,channel0interpretation,channel1interpretation,
            channel2interpretation, comments,id
        from all_events_v2
    ),
all_events_v2_3 as (
        select *,
            case 
                when recommendedlead = 1 then channel0interpretation
                when recommendedlead = 2 then channel1interpretation
                when recommendedlead = 3 then channel2interpretation 
                else CONCAT(channel0interpretation, ' ' ,
                channel1interpretation, ' ',channel2interpretation)
            end as Final_Predict
        from all_events_v2_2
    ),
nomanualorauto as (select *,
    case 
        when type = 'AFib' then 'Atrial Fibrillation'
        else type
    end as type_2
    from all_events_v2_3
    where  type not like 'Auto' and type not like 'Manual')
    ,
    count_nomanualorauto as (
    select id,type_2,Final_Predict,comments,recommendedlead,
    case
        WHEN Final_Predict LIKE '%'||type_2||'%' THEN 1
        else null
    end as predict_value
    from nomanualorauto),
count_class as (
    select *, 
    case 
        when predict_value is null then 1 
        end as predict_value_null
    from count_nomanualorauto
    
),
small_table as ( 
    select type_2,count(predict_value) as True_predict, count(predict_value_null) as False_predict
    from count_class
    group by type_2,predict_value, predict_value_null)

,total_cc as (select type_2, sum(True_predict+False_predict) as total
    from small_table
    group by type_2)

select small_table .*, total_cc.total, True_predict+ False_predict as predict_value, (True_predict+ False_predict)*100/total as percentage,
case
    when True_predict =0 then 'false'
    else 'true'
    end as predicted_state
from small_table
inner join total_cc on total_cc.type_2=small_table.type_2

```
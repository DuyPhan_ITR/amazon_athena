# Count isvalid events
```sql
with 
take_month_year as (
    SELECT month(createdat) as month_extract,year(createdat) as year_extract, *
    FROM "btcy_data_lake_processed_customer"."all_events" 
    where (month(createdat) = 8 and year(createdat) = 2022) and (eventbucket = 'Non-viable' or eventbucket = 'Viable')),

count_true_false as (
    select isvalid, count(id) as Count_value,sum(count(id)) over () ,count(id) * 100 / sum(count(id)) over () as Percentage
    from take_month_year
    group by isvalid)


select *
from count_true_false 
```
## Result
The percent isvalid event are:
1. **false**: Account for 87% (1511280)
2. **true** : Account for 12% (206914)
![](amazon_athena/Result_image/percent_result.png "Percent")

# Check the model prediction on 'Afib' ,'Tachy', 'Brady'
```sql
with 
take_month_year as (
    SELECT *
    FROM "btcy_data_lake_processed_customer"."all_events" 
    where (month(createdat) = 8 and year(createdat) = 2022) and (eventbucket = 'Non-viable' or eventbucket = 'Viable')),

join_all_events_param as (
    select take_month_year.*, all_events_params.eventid, 
                all_events_params.recommendedlead,
                all_events_params.channel0interpretation,
                all_events_params.channel1interpretation,
                all_events_params.channel2interpretation
    from take_month_year
    inner join all_events_params on take_month_year.id = all_events_params.eventid
),
take_only_event_with_MCT as (
    select join_all_events_param.*, reports.studytype
    from join_all_events_param
    inner join reports on join_all_events_param.studyfid = reports.studyfid
    where reports.studytype = 'MCT'
),
create_model_prediction as (
    select  id,type,comments,isvalid,recommendedlead,channel0interpretation,channel1interpretation,
            channel2interpretation,
            case 
                when recommendedlead = 1 then channel0interpretation
                when recommendedlead = 2 then channel1interpretation
                when recommendedlead = 3 then channel2interpretation 
                else CONCAT(channel0interpretation, ' ' ,
                channel1interpretation, ' ',channel2interpretation)
            end as Model_prediction
    from take_only_event_with_MCT
),
no_manual_pause_auto as (
    select *,
        case 
            when type = 'AFib' then 'Atrial Fibrillation'
        else type
        end as new_type
    from create_model_prediction
    where type not like '%Auto%' and type not like '%Manual%' and  type not like '%Pause%')
    
),
Final_prediction as (
select *, case 
    when Model_prediction LIKE '%'||new_type||'%' THEN 1
    end as result
from no_manual_pause_auto


select count(new_type) as total, sum(result) as true_predict,
sum(result)*100/count(new_type) as percent_predict_true_on1000
from Final_prediction
```
## Result
The accuracy score is **47%**
![](Result_image/no_auto_manu_pause_result.png "Result")

# Check the model prediction on 'Auto' ,'Manual', 'Pause'
```sql
with 
take_month_year as (
    SELECT *
    FROM "btcy_data_lake_processed_customer"."all_events" 
    where (month(createdat) = 8 and year(createdat) = 2022) and (eventbucket = 'Non-viable' or eventbucket = 'Viable')),

join_all_events_param as (
    select take_month_year.*, all_events_params.eventid, 
                all_events_params.recommendedlead,
                all_events_params.channel0interpretation,
                all_events_params.channel1interpretation,
                all_events_params.channel2interpretation
    from take_month_year
    inner join all_events_params on take_month_year.id = all_events_params.eventid
),
take_only_event_with_MCT as (
    select join_all_events_param.*, reports.studytype
    from join_all_events_param
    inner join reports on join_all_events_param.studyfid = reports.studyfid
    where reports.studytype = 'MCT'
),
create_model_prediction as (
    select  id,type,comments,isvalid,recommendedlead,channel0interpretation,channel1interpretation,
            channel2interpretation,
            case 
                when recommendedlead = 1 then channel0interpretation
                when recommendedlead = 2 then channel1interpretation
                when recommendedlead = 3 then channel2interpretation 
                else CONCAT(channel0interpretation, ' ' ,
                channel1interpretation, ' ',channel2interpretation)
            end as Model_prediction
    from take_only_event_with_MCT
),
manual_pause_auto as (
    select *
    from create_model_prediction
    where type like '%Pause%' or type like '%Auto%' or  type like '%Manual%'
),
cut_with_as_data as (
    select *, SUBSTRING(Model_prediction, 1, POSITION('at' in Model_prediction) -1) as Cut_at_predict, POSITION('at' in Model_prediction) as Locate_at,
            SUBSTRING(Model_prediction, 1, POSITION('with' in Model_prediction) -1) as Cut_with_predict, POSITION('with' in Model_prediction) as Locate_with
    from manual_pause_auto),
select_final_label as (
    select *,
        case 
            when Model_prediction like '%Could not detect beats%' then 'Artifact'
            when (Locate_with = 0 and Locate_at <> 0 )then Cut_at_predict
            when (Locate_at = 0 and Locate_with <> 0 )then Cut_with_predict
            when (Locate_at <> 0 and Locate_with <> 0 )then Cut_at_predict
            else Model_prediction
        end as Final_Predict
    from cut_with_as_data
    where Length(comments) > 2),
    
count_true_label as (
    select *, case
        WHEN comments LIKE '%'||Final_Predict||'%' THEN 1
    else null
    end as predict_value_final
from select_final_label)


select cast(count(id) as decimal(10,3)) as total, cast(sum(predict_value_final) as decimal(10,3)) as predict,
cast(sum(predict_value_final) as decimal(10,3))*100/cast(count(id) as decimal(10,3)) as percent_right
from count_true_label
```
## Result
The accuracy score is **84.9%**
![](Result_image/pause_auto_manual_result.png "Result")

# This is the code to check the model result base on MCT event:
```sql
    with all_events_v1 as (
        select all_events.*, all_events_params.eventid, 
            all_events_params.recommendedlead,
            all_events_params.channel0interpretation,
            all_events_params.channel1interpretation,
            all_events_params.channel2interpretation
        from all_events 
        inner join all_events_params on all_events.id = all_events_params.eventid
    ),
    all_events_v2 as (
        select *
        from all_events_v1
        inner join reports on all_events_v1.studyfid = reports.studyfid
        where reports.studytype = 'MCT'
    ),
    all_events_v2_2 as(
        select 
            type,recommendedlead,channel0interpretation,channel1interpretation,
            channel2interpretation
        from all_events_v2
    ),
    all_events_v2_3 as (
        select *,
            case 
                when recommendedlead = 1 then channel0interpretation
                when recommendedlead = 2 then channel1interpretation
                when recommendedlead = 3 then channel2interpretation 
                else CONCAT(channel0interpretation, ' ' ,
                channel1interpretation, ' ',channel2interpretation)
            end as Final_Predict
        from all_events_v2_2
    )
    ,
    nomanualorauto as (select *,
    case 
        when type = 'AFib' then 'Atrial Fibrillation'
        else type
    end as type_2
    from all_events_v2_3
    where  type not like 'Auto' and type not like 'Manual')
    ,
    count_nomanualorauto as (
    select type_2, Final_Predict, type,
    case
        WHEN Final_Predict LIKE '%'||type_2||'%' THEN 1
        else null
    end as predict_value
    from nomanualorauto)
    select count(type_2) as total, sum(predict_value) as true_predict,
    sum(predict_value)*1000/count(type_2) as percent_predict_true_on1000
    from count_nomanualorauto
```

